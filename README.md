# Test Docosaurus with gitlab pages

- create documentation with tool like, docusaurus, jetkyll etc
- create gitlab-ci.yml with pages job commands depend on used tool
- trigger pipeline -> static website is moved to public website
- access website - server domain is configured by admin of the self-managed instance
